<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;

class Update extends Model
{
    use HasFactory;

    public function getUpdates()
    {
        $repos = config('app.repos');

        foreach ($repos as $key => $repo) {
            echo($key);
            $client = new Client;
            $url = 'https://api.github.com/repos/' . $repo['name'] . '/releases?limit=1';
            $request = $client->request('GET', $url);

            $response = json_decode($request->getBody());
            $version  = $response[0]->tag_name;
            $html_url   = $response[0]->html_url;
            $name       = $repo["name"];
            $version    = $response[0]->tag_name;

            if (Cache::has($key)) {
                $old_version = Cache::get($key);
                if ($old_version != $version) {
                    $body = [
                        "content"=> $name . " " . $version . " has been released at " . $html_url ,
                        "tts"=> false,
                    ];

                    $body = json_encode($body);

                    $client = new Client;
                    $headers = [
                      'Content-Type' => 'application/json'
                    ];
                    $url = $repo['webhook_url'];

                    $request = $client->request('POST', $url, [
                      'body' => $body,
                      'headers' => $headers
                    ]);
                }
            } else {
                Cache::put($key, $version);
                $body = [
                    "content"=> $name . " " . $version . " has been released at " . $html_url ,
                    "tts"=> false,
                ];

                $body = json_encode($body);

                $client = new Client;
                $headers = [
                  'Content-Type' => 'application/json'
                ];
                $url = 'https://discordapp.com/api/webhooks/390242758369083392/MMKiAekqDQr4fQiyh7jAAc5dRdIW1PvOpKkBPVc5gPBipbdQKH0DMDJT1KmLcGDdkJOW';

                $request = $client->request('POST', $url, [
                  'body' => $body,
                  'headers' => $headers
                ]);
            }
        }
    }
}
